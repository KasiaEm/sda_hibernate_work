package pl.sda.hibernate;

import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;

public class Main {
    public static void main(String[] args) {
        final SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
        final EntityManager entityManager = sessionFactory.createEntityManager();
        HibernateUtils.shutdown();
    }
}
